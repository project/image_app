<?php

/**
 * Form API callback to validate the image_app settings form.
 */
function image_app_admin_settings_validate($form, &$form_state) {

  $default_image_app_size = $form_state['values']['image_app_size_default'];

  $exceed_max_msg = t('Your PHP settings limit the maximum file size per image_app to %size.', array('%size' => format_size(file_upload_max_size()))) .'<br/>';
  $more_info = t("Depending on your server environment, these settings may be changed in the system-wide php.ini file, a php.ini file in your Drupal root directory, in your Drupal site's settings.php file, or in the .htaccess file in your Drupal root directory.");

  if (!is_numeric($default_image_app_size) || ($default_image_app_size <= 0)) {
    form_set_error('image_app_size_default', t('The %role file size limit must be a number and greater than zero.', array('%role' => t('default'))));
  }
  if ($default_image_app_size * 1024 * 1024 > file_upload_max_size()) {
    form_set_error('image_app_size_default', $exceed_max_msg . $more_info);
    $more_info = '';
  }

  if(is_array($form_state['values']['roles'])) foreach ($form_state['values']['roles'] as $rid => $role) {
    $image_app_size = $form_state['values']['image_app_size_'. $rid];
    $usersize = $form_state['values']['image_app_usersize_'. $rid];

    if (!is_numeric($image_app_size) || ($image_app_size <= 0)) {
      form_set_error('image_app_size_'. $rid, t('The %role file size limit must be a number and greater than zero.', array('%role' => $role)));
    }
    if (!is_numeric($usersize) || ($usersize <= 0)) {
      form_set_error('image_app_usersize_'. $rid, t('The %role file size limit must be a number and greater than zero.', array('%role' => $role)));
    }
    if ($image_app_size * 1024 * 1024 > file_image_app_max_size()) {
      form_set_error('image_app_size_'. $rid, $exceed_max_msg . $more_info);
      $more_info = '';
    }
    if ($image_app_size > $usersize) {
      form_set_error('image_app_size_'. $rid, t('The %role maximum file size per image_app is greater than the total file size allowed per user', array('%role' => $role)));
    }
  }

  foreach (element_children($form) as $key) {
    // If there's a label they must provide either a height or width.
    if ($key != IMAGE_APP_ORIGINAL && !empty($form[$key]['label']['#value'])) {
      if (empty($form[$key]['width']['#value']) && empty($form[$key]['height']['#value'])) {
        form_set_error("image_app_sizes][$key][width", t('Must specify a width or height.'));
      }
    }
  }
}

/**
 * Menu callback for the image_app settings form.
 */
function image_app_admin_settings() {
  _image_app_check_settings();

	// To Do: Set acceptable defaults depending on what image toolkit can handle
  $image_app_extensions_default = variable_get('image_app_extensions_default', 'jpg jpeg gif png');
  $image_app_size_default = variable_get('image_app_size_default', 2);
/*
 $form['paths'] = array(
    '#type' => 'fieldset',
    '#title' => t('File paths')
  );
  $form['paths']['image_app_default_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Files Directory'),
    '#default_value' => variable_get('image_app_default_path', 'image_files'),
    '#description' => t('Subdirectory in the "%dir" directory where images will recide. Do not include trailing slash.', array('%dir' => variable_get('file_directory_path', 'files'))),
  );
*/
  $form['settings_general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
  );

  $form['settings_general']['image_app_list_default'] = array(
    '#type' => 'select',
    '#title' => t('Display images by default'),
    '#default_value' => variable_get('image_app_list_default', 1),
    '#options' => array(0 => t('No'), 1 => t('Yes')),
    '#description' => t('Display appended images when viewing a post. Can be changed for each image after upload'),
  );

  $form['settings_general']['image_app_sharpening'] = array(
    '#type' => 'select',
    '#title' => t('Apply Sharpening'),
    '#default_value' => variable_get('image_app_sharpening', 1),
    '#options' => array(0 => t('No'), 1 => t('Yes')),
    '#description' => t('Apply sharpening after resize. This will double the upload time because it utilizes the GD Library. This feature may stall the module and return a <em>System Out of Memomry</em> error if your Max Image Size limit is set too high (below).'),
  );

  $form['settings_general']['image_app_quality'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Quality'),
    '#default_value' => variable_get('image_app_quality', 80),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Enter 0-100% of the original quality, the lower the quality the smaller the file.'),
    '#field_suffix' => t('%'),
  );

  $form['settings_general']['image_app_size_default'] = array(
    '#type' => 'textfield',
    '#title' => t('Max Image Size'),
    '#default_value' => $image_app_size_default,
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('The default maximum file size for each image.'),
    '#field_suffix' => t('MB'),
  );

  $form['settings_general']['image_app_max_size'] = array('#value' => '<p>'. t('Your PHP settings limit the maximum file size per image_app to %size.', array('%size' => format_size(file_upload_max_size()))) .'</p>');

   $form['image_app_sizes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image sizes'),
    '#tree' => TRUE,
    '#theme' => 'image_app_settings_sizes_form',
    '#description' => '<p>'. t("The first three versions cannot be renamed, these are the default image versions. <br /><em>Scale image:</em> Resizes the image to fit within the given dimensions. <br /><em>Scale and crop image:</em> Crops and then resizes image to the given dimensions to maintain higher quality.")
      .'<br />'. t("Note: 'Original' dimensions will only be used to resize images when they are first uploaded. Existing originals will not be modified.").'</p><p><b>'.t("To adjust additional settings go to each individual <a href=\"@types\">content type settings'</a> page.", array('@permissions' => url('admin/user/permissions'), '@types' => url('admin/content/types'))).'</b></p>',
    '#validate' => array('image_app_settings_sizes_validate' => array()),
  );

  $sizes = image_app_get_sizes();

  // Add some empty rows for user defined sizes.
  for ($i = count($sizes); $i < 6; $i++) {
    $sizes['new'. $i] = array(
      'label' => '',
      'operation' => 'scale',
      'width' => '',
      'height' => '',
      'new' => TRUE,
    );
  }

  foreach ($sizes as $key => $size) {
    $form['image_app_sizes'][$key]['label'] = array(
      '#type' => 'textfield',
      '#default_value' => $size['label'],
      '#size' => 25,
      '#maxlength' => 32,
    );

    // For required sizes, set the value and disable the field.
    if (_image_app_is_required_size($key)) {
      $form['image_app_sizes'][$key]['label']['#disabled'] = TRUE;
      $form['image_app_sizes'][$key]['label']['#value'] = $size['label'];
      $form['image_app_sizes'][$key]['label']['#required'] = TRUE;
    }

    $form['image_app_sizes'][$key]['operation'] = array(
      '#type' => 'select',
      '#default_value' => $size['operation'],
      '#options' => array('scale' => t('Scale image'), 'scale_crop' => t('Scale and crop image')),
    );
    $form['image_app_sizes'][$key]['width'] = array(
      '#type' => 'textfield',
      '#default_value' => $size['width'],
      '#size' => 5,
      '#maxlength' => 5,
    );
    $form['image_app_sizes'][$key]['height'] = array(
      '#type' => 'textfield',
      '#default_value' => $size['height'],
      '#size' => 5,
      '#maxlength' => 5,
    );
  }

  $form['#validate'] = array('image_app_admin_settings_validate');
	$form['#submit'][] = 'image_app_admin_settings_submit';


  return system_settings_form($form);
}


function image_app_admin_settings_submit($form, &$form_state){	
	
 	#get the values that is currently in db
	$sizes = image_app_get_sizes();

	#echo $form_values['image_app_sizes']['thumbnail']['width'];
	foreach ($sizes as $key => $size) {
		#echo $key."'s width = ".$form_values['values']['image_app_sizes'][$key]['width']."<br>";
		if ($size['label'] == $form_state['values']['image_app_sizes'][$key]['label']) {
				
				#check if the values had been changed, then regenerate the derivatives
				if ($size['width'] != $form_state['values']['image_app_sizes'][$key]['width'] || $size['height'] != $form_state['values']['image_app_sizes'][$key]['height']) {
					$regenerate_derivatives = TRUE;			
					continue;
				}
		}
		
	}#end foreach
	
	if ($regenerate_derivatives == TRUE) 
			_image_app_regenerate_derivatives($form_state);

}

/**
 * Determine regenerate derivatives when values changed.
 *
 */
function _image_app_regenerate_derivatives($form_state) {
		#save the new_image_app_values first
		// Exclude unnecessary elements.
		$op = isset($form_state['values']['op']) ? $form_state['values']['op'] : '';
		unset($form_state['values']['submit'], $form_state['values']['reset'], $form_state['values']['form_id'], $form_state['values']['op'], $form_state['values']['form_token'], $form_state['values']['form_build_id']);
	
		foreach ($form_state['values'] as $key => $value) {
			if ($op == t('Reset to defaults')) {
				variable_del($key);
			}
			else {
				if (is_array($value) && isset($form_state['values']['array_filter'])) {
					$value = array_keys(array_filter($value));
				}
				variable_set($key, $value);
			}
		}
		#set the paths for building images
		$default_files_dir = file_directory_path();
		$default_image_app_dir = variable_get('image_app_default_path', 'image_files');
		
		#get all original files
		$all_original_files = file_scan_directory($default_files_dir.'/'.$default_image_app_dir, '.*', array('.', '..', 'CVS', 'versions'), 0, FALSE);
		#print_r($form_state);
	
		foreach($all_original_files as $original_files_obj) {
			#add filename into original_files
			_image_app_build_versions($default_files_dir.'/'.$default_image_app_dir.'/'.$original_files_obj->basename);
		}

}



