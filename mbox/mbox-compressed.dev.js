var mbox_loaderPath = "images/loading.gif";


//on page load call mbox_init
$(document).ready(function(){
  mbox_init('a.mbox, area.mbox, input.mbox');//pass where to apply mbox
  imgLoader = new Image();// preload image
  imgLoader.src = mbox_loaderPath;
});
//add mbox to href & area elements that have a class of .mbox
function mbox_init(domChunk){
  $(domChunk).click(function(){
  var t = this.title || this.name || null;
  var a = this.href || this.alt;
  var g = this.rel || false;
  mbox_show(t,a,g);
  this.blur();
  return false;
  });
}
function mbox_show(caption, url, imageGroup) {//function called when the user clicks on a mbox link
  try {

    var corners='<div id="MBox_corner_tl"></div><div id="MBox_corner_tr"></div><div id="MBox_corner_bl"></div><div id="MBox_corner_br"></div>';

    if (typeof document.body.style.maxHeight === "undefined") {//if IE 6
      $("body","html").css({height: "100%", width: "100%"});
      $("html").css("overflow","hidden");
      if (document.getElementById("MBox_HideSelect") === null) {//iframe to hide select elements in ie6
        $("body").append("<iframe id='MBox_HideSelect'></iframe><div id='MBox_overlay'></div><div id='MBox_window_bg'>"+corners+"</div><div id='MBox_window'></div>");
        $("#MBox_overlay").click(mbox_remove);
      }
    }else{//all others
      if(document.getElementById("MBox_overlay") === null){
        $("body").append("<div id='MBox_overlay'></div><div id='MBox_window_bg'>"+corners+"</div><div id='MBox_window'></div>");
        $("#MBox_overlay").click(mbox_remove);
      }
    }

    if(mbox_detectMacXFF()){
      $("#MBox_overlay").addClass("MBox_overlayMacFFBGHack");//use png overlay so hide flash
    }else{
      $("#MBox_overlay").addClass("MBox_overlayBG");//use background and opacity
    }

    if(caption===null){caption="";}
    $("body").append("<div id='MBox_load'><img src='"+imgLoader.src+"' /></div>");//add loader to the page
    $('#MBox_load').show();//show loader

    var baseURL;
     if(url.indexOf("?")!==-1){ //ff there is a query string involved
      baseURL = url.substr(0, url.indexOf("?"));
     }else{
         baseURL = url;
     }

     var urlString = /\.jpg$|\.jpeg$|\.png$|\.gif$|\.bmp$/;
     var urlType = baseURL.toLowerCase().match(urlString);
    if(urlType == '.jpg' || urlType == '.jpeg' || urlType == '.png' || urlType == '.gif' || urlType == '.bmp'){//code to show images

      MBox_PrevCaption = "";
      MBox_PrevURL = "";
      MBox_PrevHTML = "";
      MBox_NextCaption = "";
      MBox_NextURL = "";
      MBox_NextHTML = "";
      MBox_imageCount = "";
      MBox_FoundURL = false;
      if(imageGroup){
        MBox_TempArray = $("a[@rel="+imageGroup+"]").get();
        for (MBox_Counter = 0; ((MBox_Counter < MBox_TempArray.length) && (MBox_NextHTML === "")); MBox_Counter++) {
          var urlTypeTemp = MBox_TempArray[MBox_Counter].href.toLowerCase().match(urlString);
            if (!(MBox_TempArray[MBox_Counter].href == url)) {
              if (MBox_FoundURL) {
                MBox_NextCaption = MBox_TempArray[MBox_Counter].title;
                MBox_NextURL = MBox_TempArray[MBox_Counter].href;
                MBox_NextHTML = "<a href='#' title='Next, arrow key: right'><div id='MBox_next'>&nbsp;</div></a>";
              } else {
                MBox_PrevCaption = MBox_TempArray[MBox_Counter].title;
                MBox_PrevURL = MBox_TempArray[MBox_Counter].href;
                MBox_PrevHTML = "<a href='#' title='Previous, arrow key: left'><div id='MBox_prev'>&nbsp;</div></a>";
              }
            } else {
              if (MBox_TempArray.length>1){
	              MBox_FoundURL = true;
	              MBox_imageCount = "<div id='MBox_image_counter'>" + (MBox_Counter + 1) +" of "+ (MBox_TempArray.length)+"</div>";
              }
            }
        }
      }
      imgPreloader = new Image();
      imgPreloader.onload = function(){
      imgPreloader.onload = null;

      // Resizing large images - orginal by Christian Montoya edited by me.
      var pagesize = mbox_getPageSize();
      var x = pagesize[0] - 150;
      var y = pagesize[1] - 150;
      var imageWidth = imgPreloader.width;
      var imageHeight = imgPreloader.height;
      if (imageWidth > x) {
        imageHeight = imageHeight * (x / imageWidth);
        imageWidth = x;
        if (imageHeight > y) {
          imageWidth = imageWidth * (y / imageHeight);
          imageHeight = y;
        }
      } else if (imageHeight > y) {
        imageWidth = imageWidth * (y / imageHeight);
        imageHeight = y;
        if (imageWidth > x) {
          imageHeight = imageHeight * (x / imageWidth);
          imageWidth = x;
        }
      }
      // End Resizing

      MBox_WIDTH = imageWidth + 30;
      if(imageWidth<100) {MBox_WIDTH=130; MBox_HEIGHT = imageHeight + (caption!=''?105:80);}
      else if(imageWidth<180) {MBox_HEIGHT = imageHeight + (caption!=''?120:80);}
      else if(imageWidth<400) MBox_HEIGHT = imageHeight + (caption!=''?86:50);
      else MBox_HEIGHT = imageHeight + 46;
      MBox_TOP = (MBox_HEIGHT/(-2))-10;
      MBox_LEFT = MBox_WIDTH/(-2);
      //alert(MBox_TOP+' '+MBox_LEFT);
      MBox_dif = Math.abs(($("#MBox_window_bg").height()*$("#MBox_window_bg").width()) - ((MBox_HEIGHT+20)*MBox_WIDTH));
      $("#MBox_window_bg").animate({
		  width: MBox_WIDTH, height: (MBox_HEIGHT+20), marginTop: MBox_TOP, marginLeft: MBox_LEFT
		}, (MBox_dif>0?(MBox_dif<700000?1000:1500):0), 'easeInOutExpo', function(){
		 	$("#MBox_window").hide();
			$("#MBox_window").append("<a href='' id='MBox_ImageOff' title='Close'><img id='MBox_Image' src='"+url+"' width='"+imageWidth+"' height='"+imageHeight+"' alt='"+caption+"'/></a>" + (caption!=''?"<div id='MBox_caption'>"+caption+"</div>":"") + "<div id='MBox_closeWindow'><a href='#' id='MBox_closeWindowButton' title='Close or Press Esc Key'><div id='MBox_close_button'></div></a></div><div id='MBox_secondLine'>" + (MBox_imageCount != '' && MBox_PrevHTML != ''? MBox_PrevHTML :"<div id='MBox_last_l'></div>") + MBox_imageCount + (MBox_imageCount != '' && MBox_NextHTML != '' ? MBox_NextHTML :"<div id='MBox_last_r'></div>") + "</div>");
			$("#MBox_window").fadeIn("normal");
			$("#MBox_closeWindowButton").click(mbox_remove);


	      if (!(MBox_PrevHTML === "")) {
	        function goPrev(){
	          if($(document).unbind("click",goPrev)){$(document).unbind("click",goPrev);}
	          $("#MBox_window").fadeOut("fast", function(){
		          $("#MBox_window").remove();
		          $("body").append("<div id='MBox_window'></div>");
		          mbox_show(MBox_PrevCaption, MBox_PrevURL, imageGroup);
		          return false;
	          });
	        }
	        $("#MBox_prev").click(goPrev);
	      }

	      if (!(MBox_NextHTML === "")) {
	        function goNext(){
	          $("#MBox_window").fadeOut("fast", function(){
		          $("#MBox_window").remove();
		          $("body").append("<div id='MBox_window'></div>");
		          mbox_show(MBox_NextCaption, MBox_NextURL, imageGroup);
		          return false;
	          });
	        }
	        $("#MBox_next").click(goNext);

	      }
	      document.onkeydown = function(e){
	        if (e == null) { // ie
	          keycode = event.keyCode;
	        } else { // mozilla
	          keycode = e.which;
	        }
	        if(keycode == 27){ // close
	          mbox_remove();
	        } else if(keycode == 39 || keycode == 40){ // display previous image
	          if(!(MBox_NextHTML == "")){
	            document.onkeydown = "";
	            goNext();
	          }
	        } else if(keycode == 37 || keycode == 38){ // display next image
	          if(!(MBox_PrevHTML == "")){
	            document.onkeydown = "";
	            goPrev();
	          }
	        }
	      };
	      mbox_position();
		});
      //$("#MBox_window_bg").width(MBox_WIDTH+'px');
      //$("#MBox_window_bg").height(MBox_HEIGHT+'px');

	  //$("#MBox_window").fadeIn("normal");



      $("#MBox_load").fadeOut("fast",function(){
      	  $("#MBox_load").remove();
      	  $("#MBox_ImageOff").click(mbox_remove);
	      $("#MBox_window").css({display:"block"}); //for safari using css instead of show
	      $("#MBox_window_bg").css({display:"block"}); //for safari using css instead of show
      });

      };

      imgPreloader.src = url;
    }else{//code to show html

      var queryString = url.replace(/^[^\?]+\??/,'');
      var params = mbox_parseQuery( queryString );
      MBox_WIDTH = (params['width']*1) + 30 || 630; //defaults to 630 if no paramaters were added to URL
      MBox_HEIGHT = (params['height']*1) + 40 || 440; //defaults to 440 if no paramaters were added to URL
      ajaxContentW = MBox_WIDTH - 30;
      ajaxContentH = MBox_HEIGHT - 45;

      if(url.indexOf('MBox_iframe') != -1){// either iframe or ajax window
          urlNoQuery = url.split('MBox_');
          $("#MBox_iframeContent").remove();
          if(params['modal'] != "true"){//iframe no modal
            $("#MBox_window").append("<div id='MBox_title'><div id='MBox_ajaxWindowTitle'>"+caption+"</div><div id='MBox_closeAjaxWindow'><a href='#' id='MBox_closeWindowButton' title='Close'>close</a> or Esc Key</div></div><iframe frameborder='0' hspace='0' src='"+urlNoQuery[0]+"' id='MBox_iframeContent' name='MBox_iframeContent"+Math.round(Math.random()*1000)+"' onload='mbox_showIframe()' style='width:"+(ajaxContentW + 29)+"px;height:"+(ajaxContentH + 17)+"px;' > </iframe>");
          }else{//iframe modal
          $("#MBox_overlay").unbind();
            $("#MBox_window").append("<iframe frameborder='0' hspace='0' src='"+urlNoQuery[0]+"' id='MBox_iframeContent' name='MBox_iframeContent"+Math.round(Math.random()*1000)+"' onload='mbox_showIframe()' style='width:"+(ajaxContentW + 29)+"px;height:"+(ajaxContentH + 17)+"px;'> </iframe>");
          }
      }else{// not an iframe, ajax
          if($("#MBox_window").css("display") != "block"){
            if(params['modal'] != "true"){//ajax no modal
            $("#MBox_window").append("<div id='MBox_title'><div id='MBox_ajaxWindowTitle'>"+caption+"</div><div id='MBox_closeAjaxWindow'><a href='#' id='MBox_closeWindowButton'>close</a> or Esc Key</div></div><div id='MBox_ajaxContent' style='width:"+ajaxContentW+"px;height:"+ajaxContentH+"px'></div>");
            }else{//ajax modal
            $("#MBox_overlay").unbind();
            $("#MBox_window").append("<div id='MBox_ajaxContent' class='MBox_modal' style='width:"+ajaxContentW+"px;height:"+ajaxContentH+"px;'></div>");
            }
          }else{//this means the window is already up, we are just loading new content via ajax
            $("#MBox_ajaxContent")[0].style.width = ajaxContentW +"px";
            $("#MBox_ajaxContent")[0].style.height = ajaxContentH +"px";
            $("#MBox_ajaxContent")[0].scrollTop = 0;
            $("#MBox_ajaxWindowTitle").html(caption);
          }
      }

      $("#MBox_closeWindowButton").click(mbox_remove);

        if(url.indexOf('MBox_inline') != -1){
          $("#MBox_ajaxContent").append($('#' + params['inlineId']).children());
          $("#MBox_window").unload(function () {
            $('#' + params['inlineId']).append( $("#MBox_ajaxContent").children() ); // move elements back when you're finished
          });
          mbox_position();
          $("#MBox_load").remove();
          $("#MBox_window").css({display:"block"});
          $("#MBox_window_bg").css({display:"block"});
        }else if(url.indexOf('MBox_iframe') != -1){
          mbox_position();
          if($.browser.safari){//safari needs help because it will not fire iframe onload
            $("#MBox_load").remove();
            $("#MBox_window").css({display:"block"});
            $("#MBox_window_bg").css({display:"block"});
          }
        }else{
          $("#MBox_ajaxContent").load(url += "&random=" + (new Date().getTime()),function(){//to do a post change this load method
            mbox_position();
            $("#MBox_load").remove();
            mbox_init("#MBox_ajaxContent a.mbox");
            $("#MBox_window").css({display:"block"});
            $("#MBox_window_bg").css({display:"block"});
          });
        }

    }
    if(!params['modal']){
      document.onkeyup = function(e){
        if (e == null) { // ie
          keycode = event.keyCode;
        } else { // mozilla
          keycode = e.which;
        }
        if(keycode == 27){ // close
          mbox_remove();
        }
      };
    }

  } catch(e) {
    //nothing here
  }
}
//helper functions below
function mbox_showIframe(){
  $("#MBox_load").remove();
  $("#MBox_window").css({display:"block"});
  $("#MBox_window_bg").css({display:"block"});
}
function mbox_remove() {
   $("#MBox_imageOff").unbind("click");
  $("#MBox_closeWindowButton").unbind("click");
  $("#MBox_window").fadeOut("fast",function(){$('#MBox_window,#MBox_HideSelect').trigger("unload").unbind().remove();});
  $("#MBox_window_bg").fadeOut("fast",function(){$('#MBox_window_bg,#MBox_overlay').trigger("unload").unbind().remove();});
  $("#MBox_load").remove();
  if (typeof document.body.style.maxHeight == "undefined") {//if IE 6
    $("body","html").css({height: "auto", width: "auto"});
    $("html").css("overflow","");
  }
  document.onkeydown = "";
  document.onkeyup = "";
  return false;
}
function mbox_position() {
$("#MBox_window").css({marginLeft: '-' + parseInt((MBox_WIDTH / 2),10) + 'px', width: MBox_WIDTH + 'px'});
  if ( !(jQuery.browser.msie && jQuery.browser.version < 7)) { // take away IE6
    $("#MBox_window").css({marginTop: '-' + parseInt((MBox_HEIGHT / 2),10) + 'px'});
  }
}

function mbox_parseQuery ( query ) {
   var Params = {};
   if ( ! query ) {return Params;}// return empty object
   var Pairs = query.split(/[;&]/);
   for ( var i = 0; i < Pairs.length; i++ ) {
      var KeyVal = Pairs[i].split('=');
      if ( ! KeyVal || KeyVal.length != 2 ) {continue;}
      var key = unescape( KeyVal[0] );
      var val = unescape( KeyVal[1] );
      val = val.replace(/\+/g, ' ');
      Params[key] = val;
   }
   return Params;
}
function mbox_getPageSize(){
  var de = document.documentElement;
  var w = window.innerWidth || self.innerWidth || (de&&de.clientWidth) || document.body.clientWidth;
  var h = window.innerHeight || self.innerHeight || (de&&de.clientHeight) || document.body.clientHeight;
  arrayPageSize = [w,h];
  return arrayPageSize;
}
function mbox_detectMacXFF() {
  var userAgent = navigator.userAgent.toLowerCase();
  if (userAgent.indexOf('mac') != -1 && userAgent.indexOf('firefox')!=-1) {
    return true;
  }
}

jQuery.extend( jQuery.easing,
{
	def: 'easeInOutExpo',
	easeInOutExpo: function (x, t, b, c, d) {
		if (t==0) return b;
		if (t==d) return b+c;
		if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
		return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
	},
	easeInOutCubic: function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	},
});