(function($) {
var pause=false;
$.fn.fadeapp = function(options) {


		var settings = {
			animationtype: 'fade',
			speed: 'normal',
			timeout: 2000,
			type: 'sequence',
			containerheight: 'auto',
			runningclass: 'fadeapp',
			navigation: true
		};

		if(options)
			$.extend(settings, options);

		var elements = $(this).children();

		if (elements.length > 1) {

			$(this).css('position', 'relative');

			$(this).css('height', settings.containerheight);
			$(this).addClass(settings.runningclass);

			for ( var i = 0; i < elements.length; i++ ) {
				$(elements[i]).css('z-index', String(elements.length-i)).css('position', 'absolute');
				$(elements[i]).hide();
			};

			$.fadeapp.next(elements, settings, 0, null);

		}

		if(settings.navigation){
			var navigation = $('<div>').append(this.children().eq(1).clone().
			html('<div id="liteslide_text"></div><div id="liteslide_prev"></div><div id="liteslide_stop" class="liteslide_pause"></div><div id="liteslide_next"></div>').
			removeAttr('style').attr('id','liteslide_navigation').css('z-index',(elements.length+1))).html();
			$(this).append(navigation);
			$(this).hover(function(){$('#liteslide_navigation').fadeIn();},function(){$('#liteslide_navigation').fadeOut();});
		}
};


$.fadeapp = function() {}
$.fadeapp.next = function (elements, settings, current, last) {
	if ( settings.animationtype == 'slide' ) {
		if(last!=null)$(elements[last]).slideUp(settings.speed, $(elements[current]).slideDown(settings.speed));
		else $(elements[current]).slideDown(settings.speed);
	} else if ( settings.animationtype == 'fade' ) {
		if(last!=null)$(elements[last]).fadeOut(settings.speed);
		$(elements[current]).fadeIn(settings.speed);
	} else {
		alert('animationtype must either be \'slide\' or \'fade\'');
	};

	if ( settings.type == 'sequence' ) {
		if ( ( current + 1 ) < elements.length ) {
			current = current + 1;
			last = current - 1;
		} else {
			current = 0;
			last = elements.length - 1;
		};
	}	else if ( settings.type == 'random' ) {
		last = current;
		while (	current == last ) {
			current = Math.floor ( Math.random ( ) * ( elements.length ) );
		};
	}	else {
		alert('type must be \'sequence\' or \'random\'');
	};

	$("#liteslide_stop").unbind( "click" );
	$("#liteslide_stop").click(function(){ pause=true;$("#liteslide_stop").removeClass().addClass('liteslide_play'); });
	$("#liteslide_next").unbind( "click" );
	$("#liteslide_next").click(function(){ $.fadeapp.next(elements, settings, current, last); pause=true; $("#liteslide_stop").removeClass().addClass('liteslide_play');  });

	setTimeout((function(){
		if(!pause)$.fadeapp.next(elements, settings, current, last);
		else {
			$("#liteslide_stop").unbind( "click" );
			$("#liteslide_stop").click(function(){ $.fadeapp.next(elements, settings, current, last);pause=false;$("#liteslide_stop").removeClass().addClass('liteslide_pause'); });
		}
	}), settings.timeout);

};
})(jQuery);
